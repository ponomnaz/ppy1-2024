# Generated by Django 4.1.7 on 2023-04-25 08:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fileshare', '0004_file_file_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileuser',
            name='username',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]

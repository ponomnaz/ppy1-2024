import time
import usersSnapshots as us

if __name__ == "__main__":
    print("Starting automated user count snapshot creation:\n")
    while True:
        time.sleep(40000)
        us.createSnapshot()
        print("New snapshot created, time: {}".format(us.getCurrentTime()))

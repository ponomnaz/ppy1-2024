{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2052186d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Welcome to PPY lecture #11\n",
    "\n",
    "We will talk about code testing, introduce the PyTest and Hypothesis libraries. Also, we will have a look at some of your project — prepare questions to be discussed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aeb08c44",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Why bother?\n",
    "\n",
    "Writing tests and using a testing framework like `pytest` offers multiple significant benefits, here are some of the most important:\n",
    "\n",
    "1. **Ensuring Code Stability**: Tests provide a safety net when making changes or refactoring existing code. They help ensure that new changes don't introduce regressions or unexpected behavior by verifying that the existing functionality still works as expected.\n",
    "\n",
    "2. **Improving Code Quality**: Tests serve as a form of documentation for your code, describing how it should behave under different conditions. They encourage you to write cleaner, modular, and more maintainable code.\n",
    "\n",
    "4. **Facilitating Collaboration**: Tests make it easier for multiple developers to work on the same codebase simultaneously. They provide a common understanding of the expected behavior of the code and help prevent one developer's changes from breaking another developer's code.\n",
    "\n",
    "5. **Supporting Refactoring**: Tests provide a safety net when refactoring code by ensuring that the behavior of the code remains unchanged. They allow you to make significant changes to the codebase with confidence, knowing that the tests will catch any unintended consequences.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf70966b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Test-driven development (TDD)\n",
    "\n",
    "Software development process in which developers write tests for a piece of functionality before implementing the functionality itself. The process typically follows these steps:\n",
    "\n",
    "1. **Write a Test**: First, the developer writes a test case for the desired functionality. This test case should define the expected behavior of the code being developed.\n",
    "\n",
    "2. **Run the Test (and Watch It Fail)**: Next, the developer runs the test to ensure that it fails. Since the functionality being tested hasn't been implemented yet, the test should fail.\n",
    "\n",
    "3. **Write the Code**: The developer then writes the minimum amount of code necessary to make the test pass. This code may not be complete or optimal but should be enough to make the test pass.\n",
    "\n",
    "4. **Run the Test (and Watch It Pass)**: The developer reruns the test to ensure that it now passes with the newly implemented code.\n",
    "\n",
    "5. **Refactor (if necessary)**: Once the test passes, the developer may refactor the code to improve its design, performance, or readability while ensuring that the test continues to pass.\n",
    "\n",
    "6. **Repeat**: The process is repeated for each new piece of functionality or behavior being added to the codebase.\n",
    "\n",
    "See [Wikipedia](https://en.wikipedia.org/wiki/Test-driven_development) for more details."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ee96504",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## PyTest\n",
    "\n",
    "This is an example of traditional approach to testing — [pytest](https://docs.pytest.org/en/7.1.x/contents.html) seems to be a flexible and user-friendly option. You could use also the Python's built-in testing framework [unittest](https://docs.python.org/3/library/unittest.html).\n",
    "\n",
    "Install as usual:\n",
    "```\n",
    "pip install pytest\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1978e5ee",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Testing a calculator\n",
    "\n",
    "* Example code in: `src/calculator.py`\n",
    "* Tests in: `tests/test_calculator.py`\n",
    "* To run the tests, just run `pytest` in the root directory, and the output should look like this:\n",
    "\n",
    "```\n",
    "ppy_2024 % pytest\n",
    "================================== test session starts ==================================\n",
    "platform darwin -- Python 3.10.8, pytest-8.2.0, pluggy-1.5.0\n",
    "rootdir: /Users/matej/Documents/repos/ppy_2024\n",
    "plugins: typeguard-4.1.5, anyio-3.6.2\n",
    "collected 4 items                                                                       \n",
    "\n",
    "tests/test_calculator.py ....                                                     [100%]\n",
    "\n",
    "=================================== 4 passed in 0.01s ===================================\n",
    "```\n",
    "\n",
    "Let's have a look into the source code, and how to make the tests fail!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce469c6e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Testing a data pipeline (using a fixture)\n",
    "\n",
    "[Fixtures](https://docs.pytest.org/en/6.2.x/fixture.html) can be used to set up the initial state or environment required for testing data processing pipelines and to provide input data for testing data transformations.\n",
    "\n",
    "Here's an example of how you can use fixtures to test a simple data processing pipeline using pandas dataframes:\n",
    "* a simple pipeline is in `src/pipeline.py`\n",
    "* tests in `tests/test_pipeline.py`\n",
    "\n",
    "Let's look into them..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94686290",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Using fixtures allows you to set up and reuse input data across multiple test functions, making your test code more modular and maintainable. You can also define fixtures for setting up other resources or dependencies required for testing data processing pipelines, such as database connections, external APIs, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a79692bd",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Hypothesis\n",
    "\n",
    "[Hypothesis](https://hypothesis.readthedocs.io/en/latest/) is a property-based testing tool. It allows you to define properties that your code should satisfy, and then automatically generates a wide range of test cases to validate those properties. It's particularly useful for finding edge cases, corner cases, and unexpected behavior in your code that you might not have considered when writing traditional unit tests.\n",
    "\n",
    "While pytest and Hypothesis serve different purposes, they can be used together to create comprehensive test suites:\n",
    "\n",
    "* Use pytest as your testing framework to organize and run your tests efficiently.\n",
    "* Use Hypothesis to complement pytest by adding property-based testing to your test suite, especially for complex or critical code where finding edge cases and ensuring correctness is crucial."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28b6a1bd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's start with:\n",
    "```\n",
    "pip install hypothesis\n",
    "```\n",
    "and looking into:\n",
    "* `src/counter.py`\n",
    "* `tests/test_counter.py`\n",
    "\n",
    "Can you see the issue? If not, Hypothesis will help us find it, so let's run it:\n",
    "\n",
    "```\n",
    "pytest --hypothesis-verbosity=verbose\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a48cea5c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "> Hypothesis is useful for flushing out invariants you would never think of. Combine it with known inputs and outputs to jump start your testing for the first 80%, and augment it with Hypothesis to catch the remaining 20%.\n",
    "\n",
    "Full article, recommended: https://www.inspiredpython.com/course/testing-with-hypothesis/testing-your-python-code-with-hypothesis"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e532f42",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Project presentation\n",
    "\n",
    "Will be shared in the gitlab repo, currently in the form of merge request: https://gitlab.fjfi.cvut.cz/ksi/ppy1-2024/-/merge_requests/18"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89683edb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Inspiration for your own work\n",
    "\n",
    "Have a look at how you can better organize your tests using [OOP principles](https://docs.pytest.org/en/7.1.x/getting-started.html#group-multiple-tests-in-a-class), and try to apply them to you own project."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
